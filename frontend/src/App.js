import './App.css';
import { useEffect, useState } from 'react';
import { Routes, Route } from 'react-router-dom';
import { Home } from './pages/Home';
import { LoginPage } from './pages/Login';
import { SignUpPage } from './pages/SignUp';
import { DashboardPage } from './pages/Dashboard';
import { ProfilePage } from './pages/Profile';
import { NotesPage } from './pages/NotesPage';
import { TodosPage } from './pages/TodosPage';
import { LogoutPage } from './pages/Logout';
import { LoadingPage } from './pages/LoadingPage';
import { api } from './components/ApiClient';


function App() {
  const [userData, setUserData] = useState();
  const [loggedIn, setLoggedIn] = useState(false);
  
  
  useEffect(() => {
    const accessToken = localStorage.getItem("access");
    const refreshToken = localStorage.getItem("refresh");
    
    if (accessToken && refreshToken) {
      setLoggedIn(true);
    }
    
  }, []);
  
  useEffect(() => {
    const accessToken = localStorage.getItem("access");

    api.get("/users/get/", {
      headers: {
        Authorization: "JWT " + accessToken
      }
    })
    .then(res => {setUserData(res.data.profile)})
    .catch(error => {console.log(error);})
    
  }, [])



  return (
    <Routes>

      <Route 
        path="/" 
        element={<Home loggedIn={loggedIn} />} 
      />

      <Route 
        path="/login/" 
        element={<LoginPage setLoggedIn={setLoggedIn} />} 
      />

      <Route 
        path="/logout/" 
        element={
          loggedIn ? 
          <LogoutPage loggedIn={loggedIn} setLoggedIn={setLoggedIn} /> 
          : 
          <LoadingPage />} 
      />

      <Route 
        path="/register/" 
        element={<SignUpPage />}
      />

      <Route 
        path="/dashboard/" 
        element={
          loggedIn ? 
          <DashboardPage userData={userData} loggedIn={loggedIn}/> 
          : 
          <LoadingPage />} 
      />

      <Route 
        path="/profile/"  
        element={ 
          loggedIn ?
          <ProfilePage userData={userData} loggedIn={loggedIn} />
          :
          <LoadingPage /> } 
      />

      <Route 
        path="/notes/" 
        element={ 
          loggedIn ? 
          <NotesPage userData={userData} loggedIn={loggedIn} /> 
          : 
          <LoadingPage />} 
      />

      <Route 
        path="/todos/" 
        element={ 
        loggedIn ? 
        <TodosPage userData={userData} loggedIn={loggedIn} />
        : 
        <LoadingPage />} 
      />

    </Routes>
  );
}

export default App;
