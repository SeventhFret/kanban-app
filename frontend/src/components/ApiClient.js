import axios from 'axios';

export const apiUrl = "https://vmd108176.contaboserver.net";
const api = axios.create({
    baseURL: apiUrl,
    timeout: 5000,
});

api.interceptors.response.use(
    response => response,
    async error => {
      const originalRequest = error.config;
    
      if (error.response.status === 401) {
          const refresh_token = localStorage.getItem('refresh');

          return await api
              .post('/users/token/refresh/', {refresh: refresh_token})
              .then((response) => {
                  console.clear();

                  localStorage.setItem('access', response.data.access);
                  localStorage.setItem('refresh', response.data.refresh);

                  originalRequest.headers['Authorization'] = "JWT " + response.data.access;

                  return api(originalRequest);
              })
              .catch(err => {
                  console.log(err);
              });
      }
      return Promise.reject(error);
  }
);


export { api }