import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import { TodoDialog } from "./TodoDialog";

export function KanbanColumn(props) {
    const { handleTodosChanged, todos, columnColor, title, folderId, folders } = props;

    return (
        <Box component='div'
        display='flex' 
        flexDirection='column'
        sx={{ p: 5, 
              minWidth: '20vw',
              maxWidth: '22vw',
              overflow: 'auto',
              borderRadius: '5px', 
              backgroundColor: columnColor }}>
            <Typography variant="h6" pb="2vh" >{title}</Typography>
            <Box display="flex" flexDirection="column" gap={1}>
                { todos ? todos.map((todo) => (
                    (todo.folder === folderId) ? 
                        <TodoDialog 
                         handleTodosChanged={handleTodosChanged}
                         key={folderId} 
                         currentFolder={folderId}
                         kanban={true} 
                         todoData={todo} 
                         folders={folders} />
                    : null
                )) : null }
            </Box>
        </Box>
    )
}