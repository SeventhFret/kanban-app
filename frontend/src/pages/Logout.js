import { useEffect } from "react";
import { useNavigate } from "react-router-dom";


export function LogoutPage({ loggedIn, setLoggedIn }) {
    const navigate = useNavigate();
    
    useEffect(() => {

        if (loggedIn) {
            localStorage.clear();
        }

        setLoggedIn(false);
        navigate("/", {loggedIn});
    // eslint-disable-next-line
    }, []);


}