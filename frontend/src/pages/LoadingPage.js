import "./Home.css";
import CircularProgress from "@mui/material/CircularProgress";


export function LoadingPage() {
    return (
        <div className="flex section">
            <CircularProgress />
        </div>
    )
}