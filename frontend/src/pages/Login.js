import './Home.css';
import { LoginForm } from '../components/LoginForm';


export function LoginPage(props) {
    const { setLoggedIn } = props;

    return (
        <div className='section flex f-c'>
            <LoginForm setLoggedIn={setLoggedIn} />
        </div>
    )
}